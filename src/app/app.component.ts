import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { decrement, increment } from './NGRX/state_counter/counter.actions';
import { selectorCount } from './NGRX/state_counter/counter.selector';
import { TodoState } from './NGRX/state_todos/todo.reducer';
import { selectorAllTodos } from './NGRX/state_todos/todo.selector';
import { TodoSelector } from './NGXS/state_todos/todo-selector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'counter';
  count$!: Observable<number>;
  allTodos$!: Observable<any>;
  @Select(TodoSelector.todoItems) todoItems$: any;

  constructor(
    private store_count: Store<any>,
    private store_todos: Store<any>
  ) {
    this.count$ = store_count.select(selectorCount);
    this.allTodos$ = store_todos.select(selectorAllTodos);
  }

  increment() {
    this.store_count.dispatch(increment());
  }

  decrement() {
    this.store_count.dispatch(decrement());
  }
}
