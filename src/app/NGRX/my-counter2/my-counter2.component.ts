import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { reset } from '../state_counter/counter.actions';
import { selectorCount } from '../state_counter/counter.selector';

@Component({
  selector: 'app-my-counter2',
  templateUrl: './my-counter2.component.html',
  styleUrls: ['./my-counter2.component.css'],
})
export class MyCounter2Component implements OnInit {
  count_2$!: Observable<number>;

  constructor(private store: Store<any>) {
    this.count_2$ = this.store.select(selectorCount);
  }

  ngOnInit(): void {}

  reset() {
    this.store.dispatch(reset());
  }
}
