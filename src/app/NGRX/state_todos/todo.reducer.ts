import { createReducer, on } from '@ngrx/store';
import { Todo } from '../todo-models/todo.model';
import { addTodo, removeTodo } from './todo.actions';

export interface TodoState {
  todos: Todo[];
  error: string;
}

export const initialState: TodoState = {
  todos: [],
  error: 'null',
};

export const todoReducer = createReducer(
  initialState,
  on(addTodo, (state, { content }) => {
    if (!content) {
      return {
        ...state,
        todos: [...state.todos],
      };
    }
    return {
      ...state,
      todos: [...state.todos, { id: Date.now().toString(), content: content }],
    };
  }),
  on(removeTodo, (state, { id }) => ({
    ...state,
    todos: state.todos.filter((todo) => todo.id !== id),
  }))
);

// on(addTodo, (state, { content }) => ({
//     ...state,
//     todos: [...state.todos, { id: Date.now().toString(), content: content }],
//   })),
