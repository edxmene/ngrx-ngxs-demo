import { createAction, props } from '@ngrx/store';

export const addTodo = createAction(
  '[Todo-Input Add Todo]',
  props<{ content: string }>()
);

export const removeTodo = createAction(
  '[Todo-Item Remove Todo]',
  props<{ id: string }>()
);
