import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { removeTodo } from '../state_todos/todo.actions';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css'],
})
export class TodoItemComponent implements OnInit {
  @Input() id!: number;
  @Input() todo!: any;

  constructor(private store: Store<any>) {}

  ngOnInit(): void {}

  removeTodo() {
    this.store.dispatch(removeTodo({ id: this.id.toString() }));
  }
}
