import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { addTodo } from '../state_todos/todo.actions';

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.css'],
})
export class TodoInputComponent implements OnInit {
  constructor(private store: Store) {}
  todo: string = '';

  ngOnInit(): void {}

  onCreateAccount(accountName: string) {}

  addTodo() {
    this.store.dispatch(addTodo({ content: this.todo }));
  }
}
