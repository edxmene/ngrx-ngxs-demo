import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { divide, multiply } from '../state_counter/counter.actions';
import { selectorCount } from '../state_counter/counter.selector';

@Component({
  selector: 'app-my-counter1',
  templateUrl: './my-counter1.component.html',
  styleUrls: ['./my-counter1.component.css'],
})
export class MyCounter1Component implements OnInit {
  count_1$!: Observable<number>;

  constructor(private store: Store<any>) {
    this.count_1$ = this.store.select(selectorCount);
  }

  ngOnInit(): void {}

  multiply() {
    this.store.dispatch(multiply());
  }

  divide() {
    this.store.dispatch(divide());
  }
}
