import { TodoState } from './state_todos/todo.reducer';

export interface AppState {
  count: number;
  todos: TodoState;
}
