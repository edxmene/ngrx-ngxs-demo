import { createSelector } from '@ngrx/store';
import { AppState } from '../app.state';

export const selectCount = (state: AppState) => state.count;
export const selectorCount = createSelector(selectCount, (state) => state);
