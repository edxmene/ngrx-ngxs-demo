import { createReducer, on } from '@ngrx/store';
import {
  decrement,
  divide,
  increment,
  multiply,
  reset,
} from './counter.actions';

export const initialState = 0;

const _counterReducer = createReducer(
  initialState,
  on(increment, (state) => state + 1),
  on(decrement, (state) => state - 1),
  on(multiply, (state) => state * 2),
  on(divide, (state) => state / 2),
  on(reset, (state) => (state = 0))
);

export function counterReducer(state: any, action: any) {
  return _counterReducer(state, action);
}
