import { createAction } from '@ngrx/store';

export const increment = createAction('[app.Component] Increment');
export const decrement = createAction('[app.Component] Decrement');
export const multiply = createAction('[my-counter1 Component] Multiply');
export const divide = createAction('[my-counter1 Component] Divide');
export const reset = createAction('[my-counter2 Component] Reset');
