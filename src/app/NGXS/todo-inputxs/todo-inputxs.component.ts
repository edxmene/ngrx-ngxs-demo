import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { AddItemAction } from 'src/app/NGXS/state_todos/todo-actions';

@Component({
  selector: 'app-todo-inputxs',
  templateUrl: './todo-inputxs.component.html',
  styleUrls: ['./todo-inputxs.component.css'],
})
export class TodoInputxsComponent implements OnInit {
  todo!: string;
  constructor(private store: Store) {}

  ngOnInit(): void {}

  addTodo() {
    this.store.dispatch(new AddItemAction(this.todo));
  }
}
