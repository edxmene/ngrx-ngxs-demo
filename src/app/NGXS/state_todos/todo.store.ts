import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { removeTodo } from 'src/app/NGRX/state_todos/todo.actions';
import { Todo } from 'src/app/NGXS/todo-models/todo.model';
import { AddItemAction, RemoveItemAction } from './todo-actions';

export interface TodoStateModel {
  todos: Todo[];
}

@State<TodoStateModel>({
  name: 'todos',
  defaults: {
    todos: [],
  },
})
@Injectable()
export class TodoState {
  @Action(AddItemAction)
  addTodo(ctx: StateContext<TodoStateModel>, action: AddItemAction) {
    const { content } = action;
    if (!content) {
      return;
    }
    const state = ctx.getState();

    const todoItem: Todo = {
      id: Date.now().toString(),
      content: content,
    };

    ctx.setState({
      ...state,
      todos: [...state.todos, todoItem],
    });
  }

  @Action(RemoveItemAction)
  removeTodo(ctx: StateContext<TodoStateModel>, action: RemoveItemAction) {
    const { id } = action;
    const state = ctx.getState();
    ctx.setState({
      ...state,
      todos: state.todos.filter((todo) => todo.id !== id),
    });
  }
}
// state.todos.filter((todo) => todo.id !== id)
