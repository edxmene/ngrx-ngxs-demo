export class AddItemAction {
  static readonly type = '[Todo input component] Add item';
  constructor(public content: string) {}
}

export class RemoveItemAction {
  static readonly type = '[Todo item component] Remove item';
  constructor(public id: string) {}
}
