import { Selector } from '@ngxs/store';
import { Todo } from 'src/app/NGXS/todo-models/todo.model';
import { TodoState, TodoStateModel } from './todo.store';

export class TodoSelector {
  @Selector([TodoState])
  static todoItems(state: TodoStateModel): Todo[] {
    return state.todos;
  }
}
