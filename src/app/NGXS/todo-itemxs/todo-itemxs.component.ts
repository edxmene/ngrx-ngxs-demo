import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { RemoveItemAction } from 'src/app/NGXS/state_todos/todo-actions';

@Component({
  selector: 'app-todo-itemxs',
  templateUrl: './todo-itemxs.component.html',
  styleUrls: ['./todo-itemxs.component.css'],
})
export class TodoItemxsComponent implements OnInit {
  @Input() todo!: string;
  @Input() id!: string;
  constructor(private store: Store) {}

  ngOnInit(): void {}

  removeTodo() {
    this.store.dispatch(new RemoveItemAction(this.id.toString()));
    console.log('Remove');
  }
}
