import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { StoreModule } from '@ngrx/store';
import { counterReducer } from './NGRX/state_counter/counter.reducer';

import { AppComponent } from './app.component';
import { MyCounter1Component } from './NGRX/my-counter1/my-counter1.component';
import { MyCounter2Component } from './NGRX/my-counter2/my-counter2.component';
import { TodoInputComponent } from './NGRX/todo-input/todo-input.component';
import { TodoItemComponent } from './NGRX/todo-item/todo-item.component';
import { todoReducer } from './NGRX/state_todos/todo.reducer';
import { FormsModule } from '@angular/forms';
import { TodoInputxsComponent } from './NGXS/todo-inputxs/todo-inputxs.component';
import { TodoItemxsComponent } from './NGXS/todo-itemxs/todo-itemxs.component';
import { NgxsModule } from '@ngxs/store';
import { environment } from 'src/environments/environment';
import { TodoState } from './NGXS/state_todos/todo.store';

@NgModule({
  declarations: [
    AppComponent,
    MyCounter1Component,
    MyCounter2Component,
    TodoInputComponent,
    TodoItemComponent,
    TodoInputxsComponent,
    TodoItemxsComponent,
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({ count: counterReducer, todos: todoReducer }),
    NgxsModule.forRoot([TodoState], {
      developmentMode: !environment.production,
    }),
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
